// @ts-check

/**
 * @typedef { import('../i18n-types').Translation } Translation
 */

/** @satisfies { Translation } */
const fr = {
  app: {
    landingSection: {
      subtitle: "<i>open source</i> & auto-hébergeables",
      description:
        "C'est aussi un excellent moyen pour le créateur d'apprendre tout un tas de nouvelles choses 🚀",
      action: "Contactez-moi",
    },
    servicesSection: {
      priority: "Priorité",
    },
    timelineSection: {
      roadmap: "Carnet de route",
      subRoadmap: "(plus ou moins)",
      idea: "L'idée s'installe",
      mvp: "Premiers <i>MVP</i> (privés)",
      showcaseV1: "Showcase v1",
      gateway: "HTTP Gateway",
      firstService: "Premier Service",
      showcaseV2: "Showcase v2",
    },
  },
  footer: {
    contactEmail: "Envoyez-moi un e-mail",
  },
};

export default fr;
