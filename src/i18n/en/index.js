// @ts-check

/**
 * @typedef { import('../i18n-types').BaseTranslation } BaseTranslation
 */

/** @satisfies { BaseTranslation } */
const en = {
  app: {
    landingSection: {
      subtitle: "open source & self hostable",
      description:
        "It's also an amazing way for the creator to learn a bunch of new things 🚀",
      action: "Contact me",
    },
    servicesSection: {
      priority: "Priority",
    },
    timelineSection: {
      roadmap: "Roadmap",
      subRoadmap: "(more or less)",
      idea: "Idea sets in",
      mvp: "First (private) MVPs",
      showcaseV1: "Showcase v1",
      gateway: "HTTP Gateway",
      firstService: "First Service",
      showcaseV2: "Showcase v2",
    },
  },
  footer: {
    contactEmail: "Send me an email",
  }
};

export default en;
