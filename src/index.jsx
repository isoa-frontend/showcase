/* @refresh reload */
import { render } from "solid-js/web";
import { onMount, onCleanup, createSignal, createEffect } from "solid-js";

import { themeChange } from "theme-change";

import {
  htmlLangAttributeDetector,
  localStorageDetector,
  navigatorDetector,
} from "typesafe-i18n/detectors";
import TypesafeI18n from "intl/i18n-solid";
import { loadLocale } from "intl/i18n-util.sync";
import { detectLocale } from "intl/i18n-util";

import App from "./pages/app";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";

import "./index.css";

const Root = (props) => {
  const darkColorScheme = window.matchMedia("(prefers-color-scheme: dark)");
  const [systemIsDark, setSystemIsDark] = createSignal(darkColorScheme.matches);

  // Detect locale
  const locale = detectLocale(
    localStorageDetector, // 1st User Preferences
    navigatorDetector, // 2nd Navigator Preferences
    htmlLangAttributeDetector // 3rd Default Language
  );
  loadLocale(locale);

  createEffect(() => {
    const favicon = document.getElementById("favicon");
    if (systemIsDark()) favicon.href = "favicon_dark.svg";
    else favicon.href = "favicon_light.svg";
  });

  onMount(() => {
    themeChange();
    darkColorScheme.addEventListener("change", (e) =>
      setSystemIsDark(e.matches)
    );
  });

  onCleanup(() => {
    darkColorScheme.removeEventListener("change", (e) =>
      setSystemIsDark(e.matches)
    );
  });

  return (
    <>
      <TypesafeI18n locale={locale}>
        <Navbar />
        {/* Navbar is set to 4rem / 64px */}
        <div
          className="w-dvw fixed top-16 overflow-x-auto"
          style="height: calc(100dvh - 4rem)"
        >
          <App />
          <Footer />
        </div>
      </TypesafeI18n>
    </>
  );
};

render(() => <Root />, document.getElementById("root"));
