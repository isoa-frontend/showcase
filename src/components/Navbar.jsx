import { mdiWhiteBalanceSunny } from "@mdi/js";
import { mdiWeatherNight } from "@mdi/js";

import LocaleSelect from "./LocaleSelect";

const Navbar = (props) => {
  return (
    <navbar className="navbar h-16 px-4 fixed z-50 bg-base-100 border-b border-base-200">
      <div className="navbar-start">
        <a href={window.location.origin}>
          <svg
            className="w-11 h-11"
            viewBox="0 0 34 15"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M 7.934 1.686 C 9.577 1.686 11.12 2.281 12.277 3.377 C 13.433 4.491 14.062 5.951 14.062 7.507 C 14.062 7.815 13.798 8.084 13.453 8.084 C 13.129 8.084 12.844 7.815 12.844 7.507 C 12.844 6.258 12.337 5.086 11.404 4.203 C 10.47 3.338 9.232 2.839 7.934 2.839 C 6.615 2.839 5.377 3.338 4.444 4.203 C 3.511 5.086 3.003 6.258 3.003 7.507 C 3.003 8.276 2.333 8.929 1.502 8.929 C 0.669 8.929 0 8.276 0 7.507 C 0 5.49 0.832 3.607 2.313 2.205 C 5.418 -0.735 10.43 -0.735 13.535 2.205 C 15.036 3.607 15.848 5.49 15.848 7.507 C 15.848 9.505 15.036 11.388 13.535 12.81 C 12.033 14.232 10.045 15 7.934 15 C 7.589 15 7.305 14.75 7.305 14.424 C 7.305 14.097 7.589 13.847 7.934 13.847 C 9.72 13.847 11.404 13.175 12.662 11.984 C 13.921 10.793 14.631 9.198 14.631 7.507 C 14.631 5.817 13.921 4.222 12.662 3.012 C 10.045 0.552 5.803 0.552 3.185 3.012 C 1.927 4.222 1.238 5.817 1.238 7.507 C 1.238 7.68 1.34 7.795 1.522 7.795 C 1.684 7.795 1.785 7.699 1.785 7.507 C 1.785 5.951 2.415 4.491 3.571 3.377 C 4.728 2.281 6.29 1.686 7.934 1.686 Z M 25.527 0.245 C 27.009 0.245 28.327 1.11 28.854 2.416 L 33.948 13.963 C 34.09 14.27 33.928 14.597 33.623 14.731 C 33.299 14.846 32.933 14.712 32.812 14.404 L 27.678 2.781 C 27.313 1.955 26.461 1.417 25.527 1.417 C 24.573 1.417 23.742 1.955 23.376 2.781 L 18.75 13.252 C 18.688 13.386 18.729 13.54 18.871 13.598 C 19.014 13.655 19.156 13.598 19.216 13.444 L 23.843 2.992 C 24.126 2.358 24.797 1.955 25.527 1.955 C 26.258 1.955 26.927 2.358 27.211 3.012 L 29.605 8.41 C 29.829 8.91 29.504 9.217 29.018 9.217 L 24.147 9.217 C 23.802 9.217 23.518 8.948 23.518 8.622 C 23.518 8.295 23.802 8.045 24.147 8.045 L 28.104 8.045 L 26.075 3.473 C 25.993 3.261 25.79 3.108 25.527 3.108 C 25.263 3.108 25.06 3.261 24.979 3.473 L 20.352 13.886 C 20.048 14.616 19.176 14.962 18.405 14.673 C 17.633 14.366 17.268 13.54 17.572 12.829 L 22.199 2.416 C 22.727 1.11 24.046 0.245 25.527 0.245 Z"
              transform="matrix(1, 0, 0, 1, 0, -7.105427357601002e-15)"
              style="fill: oklch(var(--p))"
            />
          </svg>
        </a>
      </div>
      <div className="navbar-end">
        <a
          className="btn btn-ghost pt-1 prose uppercase"
          href={`${window.location.origin}/#services`}
        >
          Services
        </a>

        <label className="btn btn-ghost swap swap-rotate">
          {/* <!-- this hidden checkbox controls the swap state (rotate icon and swap theme) --> */}
          <input
            type="checkbox"
            data-toggle-theme="dark,light"
            data-act-className="ACTIVECLASS"
          />
          <svg className="swap-on fill-current w-6 h-6" viewBox="0 0 24 24">
            <path d={mdiWhiteBalanceSunny} />
          </svg>
          <svg className="swap-off fill-current w-6 h-6" viewBox="0 0 24 24">
            <path d={mdiWeatherNight} />
          </svg>
        </label>
        <LocaleSelect />
      </div>
    </navbar>
  );
};

export default Navbar;
