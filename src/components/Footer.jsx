import { mdiGitlab } from "@mdi/js";
import { mdiEmail } from "@mdi/js";
import { mdiLinkedin } from "@mdi/js";

import { useI18nContext } from "intl/i18n-solid";
import { CONTACT_EMAIL, SVG_ISOA } from "src/constants";

const Footer = (props) => {
  const { LL } = useI18nContext();

  const onSend = (event) => {
    event.preventDefault();
    window.open(`mailto:${CONTACT_EMAIL}`, "_blank");
  };

  return (
    <footer className="footer max-lg:footer-center bg-base-200 p-12">
      <aside>
        {SVG_ISOA()}

        <p>Independent Services OA</p>
      </aside>
      <nav>
        <h6 class="footer-title">Social</h6>
        <div class="grid grid-flow-col gap-4">
          <a href="https://linkedin.com/in/clement-omont" target="_blank">
            <svg viewBox="0 0 24 24" className="fill-current w-6 h-6">
              <path d={mdiLinkedin} />
            </svg>
          </a>
          <a href="https://gitlab.com/isoa-frontend/showcase" target="_blank">
            <svg viewBox="0 0 24 24" className="fill-current w-6 h-6">
              <path d={mdiGitlab} />
            </svg>
          </a>
        </div>
      </nav>
      <nav>
        <h6 class="footer-title">Contact</h6>
        <a class="link link-hover flex items-center gap-2" onClick={onSend}>
          <svg viewBox="0 0 24 24" className="fill-current w-6 h-6">
            <path d={mdiEmail} />
          </svg>
          {LL().footer.contactEmail()}
        </a>
      </nav>
    </footer>
  );
};

export default Footer;
