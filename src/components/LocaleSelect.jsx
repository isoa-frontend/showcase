import { For } from "solid-js";
import { mdiTranslate } from "@mdi/js";

import { useI18nContext } from "intl/i18n-solid";
import { locales } from "intl/i18n-util";
import { loadLocaleAsync } from "intl/i18n-util.async";
import { SHORT_TO_LONG_LOCALE } from "src/constants";

const LocaleSelect = (props) => {
  const { locale, setLocale } = useI18nContext();

  const onSelect = (event, lang) => {
    event.preventDefault();
    loadLocaleAsync(lang).then(() => {
      setLocale(lang);
      window.localStorage.setItem("lang", lang);
    });
  };

  return (
    <div className="dropdown dropdown-end">
      <div className="btn btn-ghost" tabindex="0">
        <svg className="fill-current w-6 h-6" viewBox="0 0 24 24">
          <path d={mdiTranslate} />
        </svg>
      </div>
      <div className="dropdown-content w-48 top-0 mt-16 bg-base-200 text-base-content rounded-box border border-neutral">
        <ul className="menu menu-sm gap-1">
          <For each={locales}>
            {(lang, index) => (
              <li>
                <button
                  classList={{ active: lang === locale() }}
                  onClick={(event) => onSelect(event, lang)}
                >
                  <span class="badge badge-sm badge-outline uppercase font-bold font-mono text-[.6rem] tracking-widest opacity-50">
                    {lang}
                  </span>
                  <span>{SHORT_TO_LONG_LOCALE[lang]}</span>
                </button>
              </li>
            )}
          </For>
        </ul>
      </div>
    </div>
  );
};

export default LocaleSelect;
