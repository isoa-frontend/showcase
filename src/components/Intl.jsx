const renderTranslation = (str) => {
  const parser = new DOMParser();
  const html = parser.parseFromString(str, "text/html");

  return Array.from(html.body.childNodes);
};

const Intl = (props) => {
  const str = props.children || props.str;

  if (!str) {
    throw new Error(`<Intl /> expects a children or str property`);
  }

  return renderTranslation(str);
};

export { renderTranslation };
export default Intl;
