import { For, Show } from "solid-js";

import { useI18nContext } from "intl/i18n-solid";
import { services } from "./services.json";

const ServicesSection = (props) => {
  const { LL, locale } = useI18nContext();

  return (
    <section id="services" className="hero h-full bg-base-200">
      <div className="hero-content h-full py-12 lg:py-0 lg:w-4/5 flex-col overflow-hidden">
        <article className="prose mb-12">
          <h1 className="uppercase text-primary">Services</h1>
        </article>
        <div className="flex flex-col lg:flex-row gap-4 w-full overflow-auto items-center lg:items-stretch justify-start snap-both">
          <For each={services[locale()]}>
            {(service, index) => {
              return (
                <div
                  id={`${service.uuid}`}
                  className="card w-full lg:min-w-96 snap-center bg-primary"
                >
                  <div className="card-body justify-between text-primary-content">
                    <div className="card-title prose justify-between">
                      <h3 className="uppercase text-primary-content">
                        {service.name}
                      </h3>
                    </div>
                    <div className="prose h-full text-primary-content text-center text-balance">
                      <p>{service.description}</p>
                    </div>
                    <div className="flex items-center">
                      <Show when={service.priority}>
                        <div className="badge badge-error items-end">
                          {LL().app.servicesSection.priority()}
                        </div>
                      </Show>
                      <div className="flex w-full justify-end gap-1">
                        <For each={service.categories}>
                          {(category, i) => {
                            return (
                              <div className="badge badge-outline items-end">
                                {category}
                              </div>
                            );
                          }}
                        </For>
                      </div>
                    </div>
                  </div>
                </div>
              );
            }}
          </For>
        </div>
      </div>
    </section>
  );
};

export default ServicesSection;
