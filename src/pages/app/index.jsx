import LandingSection from "./LandingSection";
import ServicesSection from "./ServicesSection";
import TimelineSection from "./TimelineSection";

const App = (props) => {
  return (
    <>
      <LandingSection />
      <ServicesSection />
      <TimelineSection />
    </>
  );
};

export default App;
