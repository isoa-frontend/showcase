import { mdiEmail } from "@mdi/js";

import { useI18nContext } from "intl/i18n-solid";
import { CONTACT_EMAIL, SVG_ISOA } from "src/constants";
import { renderTranslation } from "src/components/Intl";

const LandingSection = (props) => {
  const { LL } = useI18nContext();

  const onSend = (event) => {
    event.preventDefault();
    window.open(`mailto:${CONTACT_EMAIL}`, "_blank");
  };

  return (
    <section className="hero h-full">
      <div className="hero-content text-center w-full">
        <div className="prose max-w-xl w-full">
          <div className="px-8">{SVG_ISOA()}</div>

          <p className="uppercase text-balance">
            <b>Independent Services</b>
            <br />
            {renderTranslation(LL().app.landingSection.subtitle())}
          </p>
          <p className="text-balance">
            {LL().app.landingSection.description()}
          </p>
          <a className="btn btn-primary uppercase mt-6" onClick={onSend}>
            {LL().app.landingSection.action()}
            <svg viewBox="0 0 24 24" className="fill-current w-6 h-6">
              <path d={mdiEmail} />
            </svg>
          </a>
        </div>
      </div>
    </section>
  );
};

export default LandingSection;
