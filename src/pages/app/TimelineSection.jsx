import { mdiCheckCircle } from "@mdi/js";
import { mdiCircleSlice2 } from "@mdi/js";
import { mdiCircleSlice6 } from "@mdi/js";
import { mdiCircleOutline } from "@mdi/js";

import { useI18nContext } from "intl/i18n-solid";
import { renderTranslation } from "src/components/Intl";

const TimelineStartEnd = ({ date, key }) => {
  const { LL } = useI18nContext();

  return (
    <>
      <div className="timeline-start prose">{date}</div>
      <div className="timeline-end timeline-box prose text-start lg:text-center">
        {renderTranslation(LL().app.timelineSection[key]())}
      </div>
    </>
  );
};

const TimelineSection = (props) => {
  const { LL } = useI18nContext();

  const MiddleCheck = ({ icon = mdiCheckCircle, color = "fill-success" }) => (
    <div className="timeline-middle">
      <svg viewBox="0 0 24 24" className={`w-5 h-5 ${color}`}>
        <path d={icon} />
      </svg>
    </div>
  );

  return (
    <section id="timeline" className="hero h-full">
      <div className="hero-content flex-col text-center">
        <article className="prose mb-8">
          <h1 className="uppercase text-primary mb-0">
            {LL().app.timelineSection.roadmap()}
          </h1>
          <h4 className="mt-0">{LL().app.timelineSection.subRoadmap()}</h4>
        </article>
        <ul className="timeline timeline-vertical lg:timeline-horizontal">
          <li>
            <TimelineStartEnd date="2020" key="idea" />
            <MiddleCheck />
            <hr className="bg-success" />
          </li>

          <li>
            <hr className="bg-success" />
            <TimelineStartEnd date="2022" key="mvp" />
            <MiddleCheck />
            <hr className="bg-success" />
          </li>

          <li>
            <hr className="bg-success" />
            <TimelineStartEnd date="2024" key="showcaseV1" />
            <MiddleCheck />
            <hr className="bg-warning" />
          </li>

          <li>
            <hr className="bg-warning" />
            <TimelineStartEnd date="2024" key="gateway" />
            <MiddleCheck icon={mdiCircleSlice6} color="fill-warning" />
            <hr className="bg-base-content" />
          </li>

          <li>
            <hr className="bg-base-content" />
            <TimelineStartEnd key="firstService" />
            <MiddleCheck icon={mdiCircleSlice2} color="fill-base-content" />
            <hr className="bg-base-content" />
          </li>

          <li>
            <hr className="bg-base-content" />
            <TimelineStartEnd key="showcaseV2" />
            <MiddleCheck icon={mdiCircleOutline} color="fill-base-content" />
          </li>
        </ul>
      </div>
    </section>
  );
};

export default TimelineSection;
