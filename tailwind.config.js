/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/typography"), require("daisyui")],
  daisyui: {
    themes: [
      {
        light: {
          ...require("daisyui/src/theming/themes")["light"],
          primary: "#0040d6", // mediumslateblue
          "primary-content": "#fefefe",
          secondary: "#d69600", // goldenrod
          "secondary-content": "#161616",
          accent: "#9600d6", // darkviolet
          "accent-content": "#fefefe",

          neutral: "#2b3440",
          "neutral-content": "#d7dde4",
          "base-100": "#ffffff",
          "base-200": "#f2f2f2",
          "base-300": "#e5e6e6",
          "base-content": "#1f2937",

          info: "#00abd6", // deepskyblue
          "info-content": "#161616",
          success: "#09814A", // seagreen
          "success-content": "#fefefe",
          warning: "#CF5C36", // coral
          "warning-content": "#161616",
          error: "#931F1D", // brown
          "error-content": "#fefefe",
        },
      },
      {
        dark: {
          ...require("daisyui/src/theming/themes")["dark"],
          primary: "#d69600",
          "primary-content": "#161616",
          secondary: "#0040d6",
          "secondary-content": "#fefefe",
          accent: "#9600d6", // darkviolet
          "accent-content": "#fefefe",

          neutral: "#2a323c",
          "neutral-content": "#a6adbb",
          "base-100": "#1d232a",
          "base-200": "#191e24",
          "base-300": "#15191e",
          "base-content": "#a6adbb",

          info: "#00abd6", // deepskyblue
          "info-content": "#161616",
          success: "#09814A", // seagreen
          "success-content": "#fefefe",
          warning: "#CF5C36", // coral
          "warning-content": "#161616",
          error: "#931F1D", // brown
          "error-content": "#fefefe",
        },
      },
    ],
  },
};
