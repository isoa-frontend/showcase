# SHOWCASE

Quick Showcase for ISOA projects.<br>

> Code isn't particularly pretty! Read below.

Took this as a learning and discovery project for a few frontend "tools"..<br>
Will come back to it later, when I have a few services up and running!<br>
Trying out:

- [`SolidJS`](https://solidjs.com)
- [`ViteJS`](https://main.vitejs.dev/)
- [`daisyUI`](https://daisyui.com/) (using [`Tailwind`](https://tailwindcss.com/)).
- [`typesafe-i18n`](https://github.com/ivanhofer/typesafe-i18n) (for internationalization)<br>

`Tailwind` is very interesting, yet have to be convinced by `daisyUI`.
I struggled more often than not to circumvent `daisyUI` classes..<br>
For what I used, `SolidJS` has a few fun things, might give it a try in a bigger project.<br>
Internationalization was achieved while trying to stay framework agnostic. Doing so allows me to export some `components` for later use in other projects. Might give a shot to [`rosetta`](<[https://](https://github.com/lukeed/rosetta)>) for this purpose.

<br>

On top of my head, the project needs:

- (Better) Documentation
- Tests
- Accessibility (`aria-label` and all)
- CI/CD
  - Version bump with associated git tags
  - Docker Container Registry
- Build optimizations
  - PurgeCSS
  - Less assets / svg files or whatnot
- Bunch of code improvements

## Usage

```bash
$ npm install
```

```bash
$ docker compose up frontend
```

> Requires a [`isoa_nginx` network.](https://gitlab.com/isoa-other/nginx)
